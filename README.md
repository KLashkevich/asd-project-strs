# Taxi Project

To run the project do the following
  1) Clone the repository with `git clone https://bitbucket.org/KLashkevich/asd-project-strs.git`
  2) Go to the cloned repository project location (<cloned repo root>/asd-project-strs/taxi_project)
  3) Install dependencies with `mix deps.get`
  4) Create the database with `mix ecto.create`
  5) Migrate the schema with `mix ecto.migrate`
  6) Apply the data inserts with `mix ecto.setup`
  7) Install Node.js dependencies with `cd assets && npm install`
  8) Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

To test the project do the following:
  1) Go to the cloned repository project location (<cloned repo root>/asd-project-strs/taxi_project)
  2) To run the unit tests, execute the command `mix test`
  3) To run the unit tests coverage validation, execute the command `mix test --cover`. The coverage reports will become available in the folder <cloned repo root>/asd-project-strs/taxi_project/cover
  
# Login Credentials
  As we did not implemented registration function, here is the list of users available for login (no password is needed):
  * For `Passenger` tab: `kiryl`, `alo`, `joosep`, `kaarel`, `thanos`;
  * For `Driver` tab: `taxiboy`, `taxiboy2`, `taxiboy3`, `taxiboy4`