import Vue from "vue";
import "axios";

import passenger from "./components/passenger"
import application from "./components/application"
import driver from "./components/driver"

global.jQuery = require('jquery');
global.bootstrap = require("bootstrap")



Vue.component("driver",driver);
Vue.component("passenger", passenger);
Vue.component("application",application);



new Vue({}).$mount("#app");
