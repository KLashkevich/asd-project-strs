# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :taxi_project,
  ecto_repos: [TaxiProject.Repo]

# Configures the endpoint
config :taxi_project, TaxiProjectWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "pC1FcCBuF6rs1i2ZuWZDjnRAbFrScS2BEJnYZWe33754QxqGHsvDAIBqsarKxsoR",
  render_errors: [view: TaxiProjectWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TaxiProject.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"