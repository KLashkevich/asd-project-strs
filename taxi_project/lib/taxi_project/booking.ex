defmodule TaxiProject.Booking do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :dropoff_location, :intermediate_points, :pickup_location, :state]}
  schema "bookings" do
    field :dropoff_location, :string
    field :intermediate_points,  {:array, :string}
    field :pickup_location, :string
    field :state, :string
    belongs_to :driver, TaxiProject.Driver
    belongs_to :passenger, TaxiProject.Passenger
    belongs_to :trip_details, TaxiProject.TripDetails
    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [:pickup_location, :dropoff_location, :intermediate_points, :state, :passenger_id])
    |> validate_required([:pickup_location, :dropoff_location, :intermediate_points, :state, :passenger_id])
  end
end
