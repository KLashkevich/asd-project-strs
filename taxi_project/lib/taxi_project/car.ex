defmodule TaxiProject.Car do
  use Ecto.Schema
  import Ecto.Changeset


  schema "cars" do
    field :car_type, :string
    field :model, :string
    field :reg_number, :string
    belongs_to :driver, TaxiProject.Driver
    timestamps()
  end

  @doc false
  def changeset(car, attrs) do
    car
    |> cast(attrs, [:car_type, :model, :reg_number])
    |> validate_required([:car_type, :model, :reg_number])
  end
end
