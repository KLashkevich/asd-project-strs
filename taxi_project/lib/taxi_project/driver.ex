defmodule TaxiProject.Driver do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :car_type, :latitude, :longitude, :status, :username]}
  schema "drivers" do
    field :car_type, :string
    field :latitude, :string
    field :longitude, :string
    field :status, :string
    field :username, :string
    has_many :booking, TaxiProject.Booking

    timestamps()
  end

  @doc false
  def changeset(driver, attrs) do
    driver
    |> cast(attrs, [:car_type, :latitude, :longitude, :status, :username])
    |> validate_required([:car_type, :latitude, :longitude, :status, :username])
  end
end
