defmodule TaxiProject.Passenger do
  use Ecto.Schema
  import Ecto.Changeset


  schema "passengers" do
    field :username, :string
    field :balance, :float
    has_many :booking, TaxiProject.Booking

    timestamps()
  end

  @doc false
  def changeset(passenger, attrs) do
    passenger
    |> cast(attrs, [:username, :balance])
    |> validate_required([:username, :balance])
  end
end
