defmodule TaxiProject.Rate do
  use Ecto.Schema
  import Ecto.Changeset


  schema "rates" do
    field :code, :string
    field :name, :string
    field :value, :float
    field :start_value, :float
    field :stop_value, :float
    timestamps()
  end

  @doc false
  def changeset(rate, attrs \\ %{}) do
    rate
    |> cast(attrs, [:code, :name, :value, :start_value, :stop_value])
    |> validate_required([:code, :name, :value, :start_value, :stop_value])
  end
end
