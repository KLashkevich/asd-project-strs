defmodule TaxiProject.TripDetails do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder, only: [:id, :cost, :end_time, :start_time, :payment_method, :trip_length]}
  schema "trip_details" do
    field :cost, :string
    field :end_time, :string
    field :start_time, :string
    field :payment_method, :string
    field :trip_length, :string
    has_one :booking, TaxiProject.Booking

    timestamps()
  end

  @doc false
  def changeset(trip_details, attrs) do
    trip_details
    |> cast(attrs, [:id, :payment_method, :start_time, :end_time, :cost, :trip_length])
    |> validate_required([:payment_method, :start_time])
  end
end
