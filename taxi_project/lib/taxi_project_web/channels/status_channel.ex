defmodule TaxiProjectWeb.RoomChannel do
    use Phoenix.Channel

    def join("topic:status", _payload, socket) do
        {:ok, socket}
    end

    def handle_in("status_update", payload, socket) do
        broadcast! socket, "status_updated", payload
        {:noreply, socket}
    end

    def join("topic:bookings", _payload, socket) do
        {:ok, socket}
    end
    
    def handle_in("bookings_update", payload, socket) do
        broadcast! socket, "bookings_update", payload
        {:noreply, socket}
    end

    def join("topic:cancel", _payload, socket) do
        {:ok, socket}
    end
    
    def handle_in("cancel_booking", payload, socket) do
        broadcast! socket, "cancel_booking", payload
        {:noreply, socket}
    end

    def join("topic:accept", _payload, socket) do
        {:ok, socket}
    end
    
    def handle_in("accept_booking", payload, socket) do
        broadcast! socket, "accept_booking", payload
        {:noreply, socket}
    end


    

end