defmodule TaxiProjectWeb.API.DriverController do
    use TaxiProjectWeb, :controller
    import Ecto.Query, only: [from: 2]
    import Ecto.Changeset
    alias TaxiProject.{Repo, Booking, Driver, Car, TripDetails,Passenger}


    def set_driver_to_booking(conn, %{"params" => %{"id" => bookingId, "driver" => driverId, "confirm" => confirmed }}) do
        case Repo.get_by(Booking, id: bookingId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Driver not found"})
            record -> 
                case confirmed do 
                    "true" -> Repo.update(change record, %{driver_id: driverId, state: "driver_confirmed"})
                        put_status(conn, 201) |> json(%{record: record.state})
                    "false" -> Repo.update(change record, %{driver_id: driverId, state: "driver_declined"})
                        put_status(conn, 201) |> json(%{record: record.state})
            end
        end
    end

    def set_trip_end_time(conn, %{"params" => %{"id" => detailsID, "end" => endDate}}) do
        case Repo.get_by(TripDetails, id: detailsID) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Trip_details row not found"})
            record -> 
                Repo.update(change record, %{end_time: endDate})
                put_status(conn, 201)
                |> json(%{msg: "OK, end_time added"})
        end
    end

    def drive_to_destination(conn, %{"params" => %{"id" => bookingId}}) do
        case Repo.get_by(Booking, id: bookingId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Driver not found"})
            record -> Repo.update(change record, %{state: "driving"})
                        put_status(conn, 201) |> json(%{record: record.state})

        end
    end

    def arrived_at_destination(conn, %{"params" => %{"id" => bookingId}}) do
        case Repo.get_by(Booking, id: bookingId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Driver not found"})
            record -> Repo.update(change record, %{state: "complete"})
                        put_status(conn, 201) |> json(%{record: record.state})

        end
    end

    def driver_available(conn, %{"params" => %{"driver" => driverId}}) do
        case Repo.get_by(Driver, id: driverId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Driver not found"})
            record -> Repo.update(change record, %{status: "available"})
                        put_status(conn, 201) |> json(%{record: record.status})

        end
    end

    def driver_not_available(conn, %{"params" => %{"driver" => driverId}}) do
        case Repo.get_by(Driver, id: driverId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Driver not found"})
            record -> Repo.update(change record, %{status: "driving"})
                        put_status(conn, 201) |> json(%{record: record.status})

        end
    end

    def get_bookings(conn, %{"driver" => driver}) do
        driver = Repo.get_by(Driver, username: driver)
        if driver == nil do
            put_status(conn, 400)
                |> json(%{msg: "Driver was not found"})

        else 
            driver_id = driver.id

            query = from b in Booking, 
                    where: b.state == "pending" 
                    or (b.state == "driver_declined" and b.driver_id != ^driver_id),
                    select: b
            bookings = Repo.all(query)
                |> Enum.map(fn(info) -> Map.drop(info, [:__meta__, :__struct__,:driver,:passenger,:trip_details]) end )
            
            put_status(conn, 201) |> json(%{bookings: bookings})
        end
    end

    def get_accepted_booking(conn, %{"driv_id" => driverId}) do
        #booking = Repo.get_by(Booking, driver_id: driverId, state: "driver_confirmed")
        booking = Repo.one(
            from row in Booking,
            where: row.state == "driver_confirmed" and row.driver_id == ^driverId
            or row.state == "driving" and row.driver_id == ^driverId,
            select: row)
        # IO.inspect "-------------------------------------------------"
        # IO.inspect booking
        # IO.inspect "-------------------------------------------------"
        case booking do
            nil ->
                put_status(conn, 201)
                |> json(%{accepted_booking: "404"})
            booking ->
                put_status(conn, 201) |> json(%{accepted_booking: booking})
        end
    end

    def get_driver(conn, %{"driver" => driver}) do
        query = from c in Car, 
                    join: d in assoc(c, :driver),
                    where: d.username == ^driver,
                    select: %{id: d.id, car_type: d.car_type, latitude: d.latitude, 
                            longitude: d.longitude, status: d.status, username: d.username,
                            model: c.model, reg_number: c.reg_number}

        case Repo.one(query) do
            nil -> 
                put_status(conn, 400)
                |> json(%{msg: "User was not found"})
            record ->
                put_status(conn, 201)
                |> json(%{id: record.id, car_type: record.car_type, latitude: record.latitude, 
                    longitude: record.longitude, status: record.status, user: record.username,
                    model: record.model, reg_number: record.reg_number})
        end
    end

    def get_drivers(conn, _params) do
        query = from d in Driver, where: d.status == "available", select: d
        drivers = Repo.all(query)
        # IO.inspect drivers
        case drivers do
            [] -> 
                put_status(conn, 400) |> json(%{msg: "No drivers"})
            drivs -> 
                put_status(conn, 201) |> json(%{drivers: drivers})   
        end
    end

    def get_trip_details_id(conn, %{"booking_id" => bookID}) do
        case Repo.get_by(Booking, id: bookID) do
            nil -> 
                put_status(conn, 400)
                |> json(%{msg: "Cannot take trip_details_id from not found booking"})
            record ->
                put_status(conn, 201)
                |> json(%{details_id: record.trip_details_id})
        end
    end

    def get_trip_details(conn, %{"trip_details_id" => detailsId}) do
        case Repo.get_by(TripDetails, id: detailsId) do
            nil -> 
                put_status(conn, 400)
                |> json(%{msg: "No trip details with this trip_details id"})
            record ->
                put_status(conn, 201)
                |> json(%{details: record})
        end
    end

    def get_passenger(conn, %{"passenger_id" => passenger_id}) do
        case Repo.get_by(Passenger, id: passenger_id) do
            nil -> 
                put_status(conn, 400) |> json(%{msg: "No passenger with this id"})
            record -> put_status(conn, 201) |> json(%{passenger_name: record.username})    

        end
    end


    def set_location(conn, %{"params" => %{"driver" => driver, "lat" => lat, "long" => long}}) do
        case Repo.get_by(Driver, username: driver) do
            nil ->  
                put_status(conn, 400)
                |> json(%{msg: "User was not found"})
            record ->
                case record.status do
                    "offline" ->
                        Repo.update(change record, %{latitude: Kernel.inspect(lat), longitude: Kernel.inspect(long), status: "available"})
                    _ -> 
                        Repo.update(change record, %{latitude: Kernel.inspect(lat), longitude: Kernel.inspect(long)})
                end
                put_status(conn, 201)
                |> json(%{id: record.id, car_type: record.car_type, latitude: record.latitude, longitude: record.longitude, status: record.status, user: record.username})
        end
    end
end 