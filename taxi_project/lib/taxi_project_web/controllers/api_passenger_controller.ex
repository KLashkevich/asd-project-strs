defmodule TaxiProjectWeb.API.PassengerController do
    use TaxiProjectWeb, :controller

    import Ecto.Changeset
    alias TaxiProject.{Repo, Rate, Booking, TripDetails, Passenger}

    def go(conn, %{"booking" => booking}) do
        # IO.inspect booking
        changeset = Booking.changeset(%Booking{}, booking)
        case Repo.insert(changeset) do
            {:ok, changeset} ->
                put_status(conn, 201)
                |> json(%{id: changeset.id, pickup_location: changeset.pickup_location, dropoff_location: changeset.dropoff_location, state: changeset.state, intermediate_points: changeset.intermediate_points})
            {:error, _changeset} ->
                put_status(conn, 400)
                |> json(%{msg: "Error on booking insert"})
        end
    end

    def set_balance(conn, %{"params" => %{"userId" => id, "newBalance" => balance}})do
        case Repo.get_by(Passenger, id: id) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "User not found"})
            record -> Repo.update(change record, %{balance: balance})
                        put_status(conn, 201) |> json(%{balance: record.balance})
        end
    end

    def put_trip_details(conn, %{"trip_details" => trip_details}) do
        changeset = TripDetails.changeset(%TripDetails{}, trip_details)
        case Repo.insert(changeset) do
            {:ok, changeset} ->
                put_status(conn, 201)
                |> json(%{id: changeset.id, payment_method: changeset.payment_method, cost: changeset.cost, end_time: changeset.end_time, start_time: changeset.start_time})
            {:error, _changeset} ->
                put_status(conn, 400)
                |> json(%{msg: "Error on booking insert"})
        end
    end

    def set_details_to_booking(conn, %{"params" => %{"id" => bookingId, "details" => trip_details_id}}) do
        case Repo.get_by(Booking, id: bookingId) do 
            nil -> put_status(conn, 400)
                |> json(%{msg: "Booking not found"})
            record -> Repo.update(change record, %{trip_details_id: trip_details_id})
                        put_status(conn, 201) |> json(%{record: record.state})
        end
    end

    def get_all_bookings(conn, _params) do
        bookings = Repo.all(Booking)
        put_status(conn, 201)
            |> json(%{bookings: bookings})
    end

    def get_rate(conn, %{"code" => code}) do
        # IO.puts "----------------"
        # IO.inspect code
        # IO.inspect Repo.all(Rate)
        # IO.puts "----------------"
        # query = from r in Rate,
        #         where: r.code = code,
        #         select: r

        case Repo.get_by(Rate, code: code) do
            nil -> 
                put_status(conn, 400)
                |> json(%{msg: "Rate was not found"})
            record ->
                put_status(conn, 201)
                |> json(%{id: record.id, code: record.code, name: record.name, 
                    value: record.value, start_value: record.start_value, stop_value: record.stop_value})
        end
    end

    def cancel_booking(conn, %{"params" => %{ "booking_id" => booking_id, "user_id" => user_id }}) do
        case Repo.get_by(Booking, id: booking_id, passenger_id: user_id, state: "pending") do
            nil -> put_status(conn, 400) |> json(%{msg: "Pending booking for this user id not found"})
            record -> 
                Repo.update(change record, %{state: "user_declined"})
                put_status(conn, 201) |> json(%{record: record.state})
        end
    end

    

    def login(conn, %{"user" => user}) do
        case Repo.get_by(Passenger, username: user) do
            nil -> put_status(conn, 400) |> json(%{msg: "User was not found"})
            record -> 
                    case Repo.get_by(Booking, state: "driving", passenger_id: record.id) do    
                        nil -> 
                            case Repo.get_by(Booking, state: "driver_confirmed", passenger_id: record.id) do                              
                                nil -> 
                                    case Repo.get_by(Booking, state: "pending", passenger_id: record.id) do
                                        nil -> put_status(conn, 201) |> json(%{data: "", id: record.id, balance: record.balance})
                                        pending -> 
                                            new_data = Map.drop(pending, [:__meta__, :driver, :passenger, :trip_details])
                                            put_status(conn,201) |> json(%{data: new_data, id: record.id, balance: record.balance})
                                    end
                                    put_status(conn,201) |> json(%{data: "", id: record.id, balance: record.balance})
                                confirmed -> 
                                    new_data = Map.drop(confirmed, [:__meta__, :driver, :passenger, :trip_details])
                                    put_status(conn,201) |> json(%{data: new_data, id: record.id, balance: record.balance})
                            end
                        driving -> 
                            new_data = Map.drop(driving, [:__meta__, :driver, :passenger, :trip_details])
                            put_status(conn, 201) |> json(%{data: new_data,id: record.id, balance: record.balance})
                end
        end
    end
end