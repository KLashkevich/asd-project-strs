defmodule TaxiProjectWeb.PageController do
  use TaxiProjectWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

end
