defmodule TaxiProjectWeb.Router do
  use TaxiProjectWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TaxiProjectWeb do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
  end

  #Other scopes may use custom stacks.
  scope "/api", TaxiProjectWeb do
    pipe_through :api

  
    get "/login/user", API.PassengerController, :login
    post "/trip_details", API.PassengerController, :put_trip_details
    post "/bookings/trip_details", API.PassengerController, :set_details_to_booking
    post "/bookings", API.PassengerController, :go
    get "/bookings/all", API.PassengerController, :get_all_bookings
    get "/bookings/rate", API.PassengerController, :get_rate
    post "/bookings/cancel", API.PassengerController, :cancel_booking
    post "/user/update_balance", API.PassengerController, :set_balance
    
    get "/passenger", API.DriverController, :get_passenger
    get "/bookings", API.DriverController, :get_bookings
    get "/drivers", API.DriverController, :get_drivers
    get "/drivers/user", API.DriverController, :get_driver
    get "/bookings/trip_details_id", API.DriverController, :get_trip_details_id
    get "/bookings/trip_details", API.DriverController, :get_trip_details
    get "/bookings/booking", API.DriverController, :get_accepted_booking
    post "/drivers/user", API.DriverController, :set_location
    post "/drivers/driving", API.DriverController, :driver_not_available
    post "/bookings/driver", API.DriverController, :set_driver_to_booking
    post "/bookings/driving", API.DriverController, :drive_to_destination
    post "/bookings/complete", API.DriverController, :arrived_at_destination
    post "/drivers/available", API.DriverController, :driver_available
    post "/trip_details/end_time", API.DriverController, :set_trip_end_time


  end
end
