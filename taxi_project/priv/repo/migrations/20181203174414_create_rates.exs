defmodule TaxiProject.Repo.Migrations.CreateRates do
  use Ecto.Migration

  def change do
    create table(:rates) do
      add :code, :string
      add :name, :string
      add :value, :float
      add :start_value, :float
      add :stop_value, :float

      timestamps()
    end

  end
end
