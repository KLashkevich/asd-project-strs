defmodule TaxiProject.Repo.Migrations.CreateDrivers do
  use Ecto.Migration

  def change do
    create table(:drivers) do
      add :car_type, :string
      add :latitude, :string
      add :longitude, :string
      add :status, :string
      add :username, :string

      timestamps()
    end

  end
end
