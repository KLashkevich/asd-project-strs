defmodule TaxiProject.Repo.Migrations.CreatePassengers do
  use Ecto.Migration

  def change do
    create table(:passengers) do
      add :username, :string
      add :balance, :float

      timestamps()
    end

  end
end
