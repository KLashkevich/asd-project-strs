  defmodule TaxiProject.Repo.Migrations.CreateCars do
  use Ecto.Migration

  def change do
    create table(:cars) do
      add :car_type, :string
      add :model, :string
      add :reg_number, :string
      add :driver_id, references(:drivers, on_delete: :nothing)
      timestamps()
    end

  end
end
