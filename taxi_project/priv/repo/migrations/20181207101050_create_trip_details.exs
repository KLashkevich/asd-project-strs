defmodule TaxiProject.Repo.Migrations.CreateTripDetails do
  use Ecto.Migration

  def change do
    create table(:trip_details) do
      add :payment_method, :string
      add :start_time, :string
      add :end_time, :string
      add :cost, :string
      add :trip_length, :string

      timestamps()
    end

  end
end
