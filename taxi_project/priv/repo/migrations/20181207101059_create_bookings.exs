defmodule TaxiProject.Repo.Migrations.CreateBookings do
  use Ecto.Migration

  def change do
    create table(:bookings) do
      add :pickup_location, :string
      add :dropoff_location, :string
      add :intermediate_points, {:array, :string}
      add :state, :string
      add :driver_id, references(:drivers, on_delete: :nothing)
      add :passenger_id, references(:passengers, on_delete: :nothing)
      add :trip_details_id, references(:trip_details, on_delete: :nothing)
      timestamps()
    end

  end
end
