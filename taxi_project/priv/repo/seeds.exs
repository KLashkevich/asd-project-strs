alias TaxiProject.Repo
alias TaxiProject.Rate
alias TaxiProject.Driver
alias TaxiProject.Passenger
alias TaxiProject.Car

Repo.insert!(%Rate{code: "stand", name: "Standard", value: 0.5, start_value: 1.5, stop_value: 0.7})

driver1 = Repo.insert!(%Driver{car_type: "normal", latitude: "", longitude: "", status: "available", username: "taxiboy"})
driver2 = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "taxiboy2"})
driver3 = Repo.insert!(%Driver{car_type: "electric", latitude: "", longitude: "", status: "available", username: "taxiboy3"})
driver4 = Repo.insert!(%Driver{car_type: "normal",  latitude: "", longitude: "", status: "available", username: "taxiboy4"})
driver5 = Repo.insert!(%Driver{car_type: "normal",  latitude: "", longitude: "", status: "available", username: "viinuškiboy"})

Repo.insert!(%Car{car_type: "normal", model: "Mitsubishi Lancer Evo 8", reg_number: "NX-2389", driver: driver1})
Repo.insert!(%Car{car_type: "luxury", model: "Mercedes-Benz W213", reg_number: "XL-7777", driver: driver2})
Repo.insert!(%Car{car_type: "electric", model: "Tesla Model S", reg_number: "EL-2234", driver: driver3})
Repo.insert!(%Car{car_type: "normal", model: "Renault Logan", reg_number: "OP-4321", driver: driver4})
Repo.insert!(%Car{car_type: "normal", model: "Honda Civic", reg_number: "PL-5435", driver: driver5})

Repo.insert!(%Passenger{username: "kiryl", balance: 25.0})
Repo.insert!(%Passenger{username: "alo", balance: 10.0})
Repo.insert!(%Passenger{username: "joosep", balance: 500.0})
Repo.insert!(%Passenger{username: "kaarel", balance: 50.50})
Repo.insert!(%Passenger{username: "thanos", balance: 5000.95})
