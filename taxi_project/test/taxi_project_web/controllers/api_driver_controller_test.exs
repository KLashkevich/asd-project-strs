defmodule TaxiProjectWeb.API.DriverControllerTest do
  use TaxiProjectWeb.ConnCase

  alias TaxiProject.{Repo, Rate, Booking, State, TripDetails, Passenger, Driver, Car}

  #POST  /api/bookings/driver           TaxiProjectWeb.API.DriverController :set_driver_to_booking
#   test "POST /api/bookings/driver", %{conn: conn} do
#     driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
#     driver_id1 = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy1"}).id;
#     passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
#     booking_id = Repo.insert!(%Booking{driver_id: driver_id1, passenger_id: passenger_id,
#         state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
#         dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

#     request = %{"params" => %{"id" => booking_id, "driver" => driver_id, "confirm" => "true"}}

#     response =
#         conn
#         |> post(driver_path(conn, :set_driver_to_booking, request))
#         |> json_response(201)

#     expected = %{"record" => "pending"}

#     assert response == expected
#   end

#   test "POST /api/bookings/driver - declined", %{conn: conn} do
#     driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
#     driver_id1 = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy1"}).id;
#     passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
#     booking_id = Repo.insert!(%Booking{driver_id: driver_id1, passenger_id: passenger_id,
#         state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
#         dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

#     request = %{"params" => %{"id" => booking_id, "driver" => driver_id, "confirm" => "true"}}

#     response =
#         conn
#         |> post(driver_path(conn, :set_driver_to_booking, request))
#         |> json_response(201)

#     expected = %{"record" => "pending"}

#     assert response == expected
#   end

  test "POST /api/bookings/driver - not found", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    driver_id1 = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy1"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

    request = %{"params" => %{"id" => -1, "driver" => driver_id, "confirm" => "true"}}

    response =
        conn
        |> post(driver_path(conn, :set_driver_to_booking, request))
        |> json_response(400)

    expected = %{"msg" => "Driver not found"}

    assert response == expected
  end
  
  
  test "POST /api/bookings/driving", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"params" => %{"id" => booking_id}}

    response =
        conn
        |> post(driver_path(conn, :drive_to_destination, request))
        |> json_response(201)

    expected = %{"record" => "pending"}

    assert response == expected
  end

  test "POST /api/bookings/driving fail", %{conn: conn} do
    # driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    # passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    # booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
    #     state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
    #     dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"params" => %{"id" => -1}}

    response =
        conn
        |> post(driver_path(conn, :drive_to_destination, request))
        |> json_response(400)

    expected = %{"msg" => "Driver not found"}

    assert response == expected
  end

  test "POST /api/bookings/complete", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"params" => %{"id" => booking_id}}

    response =
        conn
        |> post(driver_path(conn, :arrived_at_destination, request))
        |> json_response(201)

    expected = %{"record" => "pending"}

    assert response == expected
  end

  test "POST /api/bookings/complete fail", %{conn: conn} do
    request = %{"params" => %{"id" => -1}}

    response =
        conn
        |> post(driver_path(conn, :arrived_at_destination, request))
        |> json_response(400)

    expected = %{"msg" => "Driver not found"}

    assert response == expected
  end

  test "POST /api/bookings/available", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "Dummy"}).id;

    request = %{"params" => %{"driver" => driver_id}}

    response =
        conn
        |> post(driver_path(conn, :driver_available, request))
        |> json_response(201)

    expected = %{"record" => "available"}

    assert response == expected
  end

  test "POST /api/drivers/available fail", %{conn: conn} do
    request = %{"params" => %{"driver" => -1}}

    response =
        conn
        |> post(driver_path(conn, :driver_available, request))
        |> json_response(400)

    expected = %{"msg" => "Driver not found"}

    assert response == expected
  end

  test "POST /api/drivers/driving", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "Dummy"}).id;

    request = %{"params" => %{"driver" => driver_id}}

    response =
        conn
        |> post(driver_path(conn, :driver_not_available, request))
        |> json_response(201)

    expected = %{"record" => "available"}

    assert response == expected
  end

  test "POST /api/drivers/driving fail", %{conn: conn} do
    request = %{"params" => %{"driver" => -1}}

    response =
        conn
        |> post(driver_path(conn, :driver_not_available, request))
        |> json_response(400)

    expected = %{"msg" => "Driver not found"}

    assert response == expected
  end

  test "GET /api/bookings", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "pending", passenger_id: passenger_id})
    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Lounakeskus",
        intermediate_points: [], state: "user_declined", passenger_id: passenger_id})
    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Lounakeskus",
        intermediate_points: [], state: "driver_declined", passenger_id: passenger_id, driver_id: driver_id})
    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Lounakeskus",
        intermediate_points: [], state: "complete", passenger_id: passenger_id})

    request = %{"driver" => "Dummy"}

    response =
        conn
        |> get(driver_path(conn, :get_bookings, request))
        |> json_response(201)

    #IO.inspect response
    expected = 1

    assert length(Map.get(response, "bookings")) == expected

  end

  test "GET /api/bookings driver not found", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;

    request = %{"driver" => "Dummy1"}

    response =
        conn
        |> get(driver_path(conn, :get_bookings, request))
        |> json_response(400)

    #IO.inspect response
    expected = %{"msg" => "Driver was not found"}

    assert response == expected

  end

  test "GET /api/drivers/user", %{conn: conn} do
    driver = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"});
    car = Repo.insert!(%Car{car_type: "normal", model: "Mitsubishi Lancer Evo 8", reg_number: "NX-2389", driver: driver})

    request = %{"driver" => "Dummy"}

    response =
        conn
        |> get(driver_path(conn, :get_driver, request))
        |> json_response(201)

    #IO.inspect response
    expected = "Dummy"
    expected_num = "NX-2389"

    assert Map.get(response, "user") == expected
    assert Map.get(response, "reg_number") == expected_num

  end

  test "GET /api/drivers/user not found", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;

    request = %{"driver" => "Jimmy Notfound"}

    response =
        conn
        |> get(driver_path(conn, :get_driver, request))
        |> json_response(400)

    #IO.inspect response
    expected = %{"msg" => "User was not found"}

    assert response == expected

  end


    test "GET /bookings/trip_details_id", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
        trip_details_id = Repo.insert!(%TripDetails{cost: "666", end_time: "", start_time: "", payment_method: ""}).id;
        passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

        booking_id = Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "pending", passenger_id: passenger_id, driver_id: driver_id,
        trip_details_id: trip_details_id}).id;

        request = %{"booking_id" => booking_id}

        response =
            conn
            |> get(driver_path(conn, :get_trip_details_id, request))
            |> json_response(201)

        #IO.inspect response
        expected = %{"details_id" => trip_details_id}

        assert response == expected

    end

    test "GET /bookings/trip_details_id not found booking", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
        trip_details_id = Repo.insert!(%TripDetails{cost: "666", end_time: "", start_time: "", payment_method: ""}).id;
        passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

        Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "pending", passenger_id: passenger_id, driver_id: driver_id,
        trip_details_id: trip_details_id});

        request = %{"booking_id" => -1}

        response =
            conn
            |> get(driver_path(conn, :get_trip_details_id, request))
            |> json_response(400)

        #IO.inspect response
        expected = %{"msg" => "Cannot take trip_details_id from not found booking"}

        assert response == expected

    end

    test "POST /trip_details/end_time", %{conn: conn} do
        trip_details_id = Repo.insert!(%TripDetails{cost: "666", end_time: "", start_time: "", payment_method: ""}).id;

        request = %{"params" => %{"id" => trip_details_id, "end" => "1234567"}}
    
        response =
            conn
            |> post(driver_path(conn, :set_trip_end_time, request))
            |> json_response(201)
    
        expected = %{"msg" => "OK, end_time added"}
    
        assert response == expected
    end

    test "POST /trip_details/end_time failed time adding", %{conn: conn} do
        request = %{"params" => %{"id" => -1, "end" => "1234567"}}
    
        response =
            conn
            |> post(driver_path(conn, :set_trip_end_time, request))
            |> json_response(400)
    
        expected = %{"msg" => "Trip_details row not found"}
    
        assert response == expected
    end

    test "get /booking/trip_details gets all trip details", %{conn: conn} do
        trip = %TripDetails{cost: "10", end_time: "", start_time: "", payment_method: "cash", trip_length: "100"}
        trip_details_id = Repo.insert!(trip).id;
        request = %{"trip_details_id" => trip_details_id}

        response =
            conn
            |> get(driver_path(conn, :get_trip_details, request))
            |> json_response(201)

       

        expected = %{"details" => %{"cost" =>  "10", "end_time" =>  "", "start_time" => "", "payment_method" => "cash", "trip_length" => "100", "id" => trip_details_id}}

        assert response == expected
    end

    test "get /booking/trip_details trip details not found -> error", %{conn: conn} do

        request = %{"trip_details_id" => 100}
        response =
            conn
            |> get(driver_path(conn, :get_trip_details, request))
            |> json_response(400)
        expected = %{"msg" => "No trip details with this trip_details id"}

        assert response == expected
    end

    test "get /api/bookings/booking", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
        trip_details_id = Repo.insert!(%TripDetails{cost: "666", end_time: "", start_time: "", payment_method: ""}).id;
        passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

        booking_id = Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "driver_confirmed", passenger_id: passenger_id, driver_id: driver_id,
        trip_details_id: trip_details_id}).id;

        request = %{"driv_id" => driver_id}
        response =
            conn
            |> get(driver_path(conn, :get_accepted_booking, request))
            |> json_response(201)
        expected = %{"accepted_booking" => "404"}
        assert response != expected
    end

    test "get /api/bookings/booking - not found", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
        trip_details_id = Repo.insert!(%TripDetails{cost: "666", end_time: "", start_time: "", payment_method: ""}).id;
        passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

        booking_id = Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "driver_declined", passenger_id: passenger_id, driver_id: driver_id,
        trip_details_id: trip_details_id}).id;

        request = %{"driv_id" => driver_id}
        response =
            conn
            |> get(driver_path(conn, :get_accepted_booking, request))
            |> json_response(201)
        expected = %{"accepted_booking" => "404"}
        assert response == expected
    end

    test "get /api/drivers", %{conn: conn} do
        Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"});
        Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "Dummy2"});
        Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "offline", username: "Dummy3"});

        response =
            conn
            |> get(driver_path(conn, :get_drivers))
            |> json_response(201)

        expected = 1
        assert length(Map.get(response, "drivers")) == expected
    end

    test "get /api/drivers - no drivers", %{conn: conn} do
        Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"});
        Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "offline", username: "Dummy3"});

        response =
            conn
            |> get(driver_path(conn, :get_drivers))
            |> json_response(400)

        expected = %{"msg" => "No drivers"}
        assert response == expected
    end

    #POST  /api/drivers/user              TaxiProjectWeb.API.DriverController :set_location
    test "POST /api/drivers/user", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "offline", username: "Dummy"}).id;

        request = %{"params" => %{"driver" => "Dummy", "lat" => "54.43543", "long" => "32.435"}}

        response =
            conn
            |> post(driver_path(conn, :set_location, request))
            |> json_response(201)

        expected = %{"id" => driver_id, "car_type" => "luxury", "latitude" => "",
                    "longitude" => "", "status" => "offline", "user" => "Dummy"}

        assert response == expected
    end

    test "POST /api/drivers/user found available", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "Dummy"}).id;

        request = %{"params" => %{"driver" => "Dummy", "lat" => "54.43543", "long" => "32.435"}}

        response =
            conn
            |> post(driver_path(conn, :set_location, request))
            |> json_response(201)

        expected = %{"id" => driver_id, "car_type" => "luxury", "latitude" => "",
                    "longitude" => "", "status" => "available", "user" => "Dummy"}

        assert response == expected
    end

    test "POST /api/drivers/user not found", %{conn: conn} do
        driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "available", username: "Dummy"}).id;

        request = %{"params" => %{"driver" => "Dummy1", "lat" => "54.43543", "long" => "32.435"}}

        response =
            conn
            |> post(driver_path(conn, :set_location, request))
            |> json_response(400)

        expected = %{"msg" => "User was not found"}

        assert response == expected
    end

end