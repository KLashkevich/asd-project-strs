defmodule TaxiProjectWeb.API.PassengerControllerTest do
  use TaxiProjectWeb.ConnCase

  alias TaxiProject.{Repo, Rate, Booking, State, TripDetails, Passenger, Driver}

  test "GET /api/bookings/rate", %{conn: conn} do
    Repo.insert!(%Rate{code: "stand", name: "standard", value: 0.5, start_value: 2.5, stop_value: 0.8})
    response =
        conn
        |> get(passenger_path(conn, :get_rate, %{"code" => "stand"}))
        |> json_response(201)

    expected = "standard"

    assert Map.get(response, "name") == expected
  end

  test "GET /api/bookings/rate fails", %{conn: conn} do
    response =
        conn
        |> get(passenger_path(conn, :get_rate, %{"code" => "stand"}))
        |> json_response(400)

    assert Map.get(response, "msg") == "Rate was not found"
  end

#   test "POST /user/update_balance", %{conn: conn} do

#     request = %{"params" => %{"userId" => -1, "newBalance" => nil}}

#     response =
#         conn
#         |> get(passenger_path(conn, :login, request))
#         |> json_response(400)

#     expected = %{"msg" => "User not found"}

#     assert response == expected 
#   end

  test "GET /api/bookings", %{conn: conn} do
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Narva mnt 25",
        intermediate_points: [], state: "pending", passenger_id: passenger_id})
    Repo.insert!(%Booking{pickup_location: "Liivi 2", dropoff_location: "Lounakeskus",
        intermediate_points: [], state: "user_declined", passenger_id: passenger_id})

    response =
        conn
        |> get(passenger_path(conn, :get_all_bookings))
        |> json_response(201)

    #IO.inspect response
    expected = 2

    assert length(Map.get(response, "bookings")) == expected
  end

#   test "POST /api/bookings", %{conn: conn} do
#     driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
#     passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

#     request = %{"booking" => %{"booking" => %{"driver_id" => driver_id, 
#                                             "dropoff_location" => "Narva maantee 25, 51009 Tartu", 
#                                             "intermediate_points" => ["Kompanii 2, Tartu"], 
#                                             "passenger_id" => passenger_id, 
#                                             "pickup_location" => "Julius Kuperjanovi 5a, 50409 Tartu", 
#                                             "state" => "pending"}}}
#     IO.inspect request

#     response =
#         conn
#         |> post(passenger_path(conn, :go, request))
#         |> json_response(201)

#     #IO.inspect response
#     expected = %{"msg" => "Liivi 2"}

#     assert response == expected
#   end

  test "POST /api/bookings fails", %{conn: conn} do
    request = %{"booking" => %{"booking" => %{"driver_id" => -1, 
                                            "dropoff_location" => "Narva maantee 25, 51009 Tartu", 
                                            "intermediate_points" => ["Kompanii 2, Tartu"], 
                                            "passenger_id" => -1, 
                                            "pickup_location" => "Julius Kuperjanovi 5a, 50409 Tartu", 
                                            "state" => "pending"}}}
    response =
        conn
        |> post(passenger_path(conn, :go, request))
        |> json_response(400)
    assert Map.get(response, "msg") == "Error on booking insert"
  end

  test "POST /api/trip_details", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{trip_details: [cost: "7.34",  
                                            start_time: "10:00", 
                                            end_time: "10:30", 
                                            payment_method: "card"]}

    #TripDetails
    response =
        conn
        |> post(passenger_path(conn, :put_trip_details, request))
        |> json_response(201)

    expected = "7.34"

    assert Map.get(response, "cost") == expected
  end

  test "POST /api/trip_details fails", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{trip_details: [cost: nil,  
                                            start_time: nil, 
                                            payment_method: nil,
                                            booking_id: -1]}

    #TripDetails
    response =
        conn
        |> post(passenger_path(conn, :put_trip_details, request))
        |> json_response(400)

    expected = %{"msg" => "Error on booking insert"}
    assert response == expected
  end

  test "POST /api/bookings/cancel", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;
    request = %{"params" => %{"booking_id" => booking_id, "user_id" => passenger_id}}
    response =
        conn
        |> post(passenger_path(conn, :cancel_booking, request))
        |> json_response(201)
    expected = %{"record" => "pending"}
    
    assert response == expected
  end

  test "POST /api/bookings/cancel fails", %{conn: conn} do
    request = %{params: [booking_id: -1,  user_id: -1]}
    response =
        conn
        |> post(passenger_path(conn, :cancel_booking, request))
        |> json_response(400)

    expected = %{"msg" => "Pending booking for this user id not found"}

    assert response == expected
  end

  test "POST /api/bookings/trip_details", %{conn: conn} do
    request = %{params: [id: -1,  details: -1]}
    response =
        conn
        |> post(passenger_path(conn, :set_details_to_booking, request))
        |> json_response(400)

    expected = %{"msg" => "Booking not found"}

    assert response == expected
  end

  test "POST /api/login/user not found", %{conn: conn} do
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

    request = %{"user" => "Mate"}

    response =
        conn
        |> get(passenger_path(conn, :login, request))
        |> json_response(400)

    expected = %{"msg" => "User was not found"}

    assert response == expected
  end

  test "POST /api/login/user found - driving", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "driving", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"user" => "Dummy"}

    response =
        conn
        |> get(passenger_path(conn, :login, request))
        |> json_response(201)

    assert response != nil
  end

  test "POST /api/login/user found - driver_confirmed", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "driver_confirmed", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"user" => "Dummy"}

    response =
        conn
        |> get(passenger_path(conn, :login, request))
        |> json_response(201)

    assert response != nil
  end

  test "POST /api/login/user found - pending", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;
    booking_id = Repo.insert!(%Booking{driver_id: driver_id, passenger_id: passenger_id,
        state: "pending", pickup_location: "Julius Kuperjanovi 5a, 50409 Tartu",
        dropoff_location: "Narva maantee 25, 51009 Tartu", intermediate_points: []}).id;

    request = %{"user" => "Dummy"}

    response =
        conn
        |> get(passenger_path(conn, :login, request))
        |> json_response(201)

    assert response != nil
  end

  test "POST /api/login/user found - no active cookings", %{conn: conn} do
    driver_id = Repo.insert!(%Driver{car_type: "luxury", latitude: "", longitude: "", status: "", username: "Dummy"}).id;
    passenger_id = Repo.insert!(%Passenger{username: "Dummy"}).id;

    request = %{"user" => "Dummy"}

    response =
        conn
        |> get(passenger_path(conn, :login, request))
        |> json_response(201)

    assert response != nil
  end

  #POST  /api/user/update_balance       TaxiProjectWeb.API.PassengerController :set_balance
  test "POST /api/user/update_balance fails", %{conn: conn} do
    request = %{"params" => %{"userId" => -1, "newBalance" => 10}}

    response =
        conn
        |> post(passenger_path(conn, :set_balance, request))
        |> json_response(400)

    expected = %{"msg" => "User not found"}

    assert response == expected
  end
end