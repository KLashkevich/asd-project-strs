defmodule TaxiProjectWeb.PageControllerTest do
  use TaxiProjectWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to STRS!"
  end
end
